package com.renomeararquivos.resources;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import com.renomeiaarquivos.ui.TelaPrincipal;

public class AlteraNomeArquivo {
	private static String diretorio = "";
	private static String remover = "";
	private static TelaPrincipal principal = new TelaPrincipal();
	
	public static void main(String[] args) {

		principal.montaTela();
		principal.getBtnExecutar().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				diretorio = principal.getDiretorioTxtField().getText();
				remover = principal.getRemoverTxtField().getText();
				if(!diretorio.trim().equals("") && diretorio != null){
					try {
						visualizarArquivos();
						principal.getDiretorioTxtField().setText("");
						principal.getRemoverTxtField().setText("");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					principal.getDescricaoTxtField().setText("Insira o Diret�rio");
				}
			}
		});
	}
	
	
	public static void visualizarArquivos() throws IOException {

		File file = new File(diretorio);
		File afile[] = file.listFiles();
		int cont = 0;
		
		for(int i = 0; i< afile.length; i++){
			File arquivo = afile[i];
			//sequencia de caracteres em comum que deseja retirar dos arquivos
			if(!remover.equals("") && remover != null){
				if(arquivo.getName().contains(remover)){
					principal.getDescricaoTxtField().setText(arquivo.getName());
					File newFile = new File (arquivo.getAbsolutePath().replace(remover, ""));
					arquivo.renameTo(newFile);
					cont++;				
				}
			}
		}
		principal.getDescricaoTxtField().setText("Arquivos Renomeados: " + cont + " \n- Para visualiz�-los v� at�: " + diretorio);
	}

}
