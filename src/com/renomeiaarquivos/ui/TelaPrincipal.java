package com.renomeiaarquivos.ui;

import java.awt.*;

import javafx.*;
public class TelaPrincipal extends Frame {

	Label lblDiretorio = new Label("Diretório");
	Label lblRemover = new Label("Remover");
	
	TextField diretorioTxtField = new TextField();
	TextField removerTxtField = new TextField();
	
	TextArea descricaoTxtField = new TextArea();
	Button btnExecutar = new Button("Executar");
	
	public void montaTela(){
		setBounds();
		addComponentes();
		
		centreWindow(this);
		setTitle("Renomear Arquivos");
		setSize(420,300);  
		setLayout(null);  
		setVisible(true);  
		
	}
	
	private void centreWindow(Window frame) {
	    Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
	    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
	    frame.setLocation(x, y);
	}
	
	private void setBounds(){
		lblDiretorio.setBounds(20, 70, 50, 25);
		diretorioTxtField.setBounds(80,70,250,25);
		lblRemover.setBounds(20, 100, 55, 25);
		removerTxtField.setBounds(80, 100, 100, 25);
		btnExecutar.setBounds(280,120,70,25);
		descricaoTxtField.setBounds(20, 160, 350, 100);
	}
	
	
	private void addComponentes(){
		add(lblRemover);
		add(lblDiretorio);
		add(diretorioTxtField);
		add(removerTxtField);
		add(btnExecutar);
		add(descricaoTxtField);
	}

	public TextField getDiretorioTxtField() {
		return diretorioTxtField;
	}
	
	public TextField getRemoverTxtField() {
		return removerTxtField;
	}

	public Button getBtnExecutar() {
		return btnExecutar;
	}

	public TextArea getDescricaoTxtField() {
		return descricaoTxtField;
	}
	
}
