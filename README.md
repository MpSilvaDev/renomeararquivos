**Renomeia Arquivos**

## O que é? 
 Muitas vezes você se ve diante do seguinte cenário: alguns arquivos em alguma pasta física do seu computador estão com algum 
 conjunto de caracteres indesejado por exemplo, 'arquivo1-XXX.jpg', 'arquivo2-XXX.jpg', 'arquivo3-XXX.jpg' etc... 
 Retirar esses caracteres de cada arquivo é uma tarefa horrível, então desenvolvi esse programa para nos ajudar quando nos depararmos com esta situação.
 
 
## Como funciona? 
 Execute a classe principal e será mostrado uma tela para que você informe o caminho completo da pasta onde estão seus arquivos que deseja alterar, no campo
 abaixo informe qual é o conjunto de caracteres que deseja remover dos arquivos e depois clique em executar.
 
